part of 'home_cubit.dart';

abstract class DogsState extends GeneralHomeState {}

class DogsInitial extends DogsState {
  @override
  List<Object> get props => [];
}

class DogsSuccess extends DogsState {
  DogsSuccess(this.dogsModel);

  final DogsModel dogsModel;

  @override
  List<Object> get props => [dogsModel];
}

class DogsLoading extends DogsState {
  @override
  List<Object> get props => [];
}

class DogsFailure extends DogsState {
  DogsFailure(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}
