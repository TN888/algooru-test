import 'package:algooru_test/features/home/repository/home_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

import '../models/dogs_model.dart';

part 'home_state.dart';
part 'dogs_state.dart';
part 'cats_state.dart';

class HomeCubit extends Cubit<GeneralHomeState> {
  HomeCubit(
    this.homeRepository,
  ) : super(
          HomeInitial(),
        );

  IHomeRepository homeRepository;

  Future<void> getDogs() async {
    emit(DogsLoading());

    try {
      final response = await homeRepository.getDogs();

      //This just to emulate the API response.
      final isSuccess = response is DogsModel;

      if (isSuccess) {
        emit(DogsSuccess(response));
      } else {
        emit(DogsFailure('Something went wrong'));
      }
    } catch (e, stackTrace) {
      debugPrint('Error $stackTrace');
      emit(DogsFailure('Something went wrong'));
    }
  }

  Future<void> getOneCat(int index) async {
    emit(CatsLoading(index));

    try {
      final response = await homeRepository.getOneCat();

      //This just to emulate the API response.
      final isSuccess = response is String;

      if (isSuccess) {
        emit(CatsSuccess(response, index));
      } else {
        emit(CatsFailure('Something went wrong', index));
      }
    } catch (e, stackTrace) {
      debugPrint('Error $stackTrace');
      emit(CatsFailure('Something went wrong', index));
    }
  }
}
