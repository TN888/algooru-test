part of 'home_cubit.dart';

class CatsState extends GeneralHomeState {
  const CatsState(this.index);

  final int index;
}

class CatsInitial extends CatsState {
  const CatsInitial(int index) : super(index);

  @override
  List<Object> get props => [index];
}

class CatsSuccess extends CatsState {
  const CatsSuccess(this.oneCat, int index) : super(index);

  final String oneCat;

  @override
  List<Object> get props => [oneCat, index];
}

class CatsLoading extends CatsState {
  const CatsLoading(int index) : super(index);

  @override
  List<Object> get props => [index];
}

class CatsFailure extends CatsState {
  const CatsFailure(this.message, int index) : super(index);

  final String message;

  @override
  List<Object> get props => [message, index];
}
