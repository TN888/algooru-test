import 'package:flutter/material.dart';
import 'package:shimmer_animation/shimmer_animation.dart';

class ImageLoadingWidget extends StatelessWidget {
  const ImageLoadingWidget({
    Key? key,
    this.height,
  }) : super(key: key);

  final double? height;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(16),
      child: Shimmer(
        color: Colors.green,
        colorOpacity: 0.5,
        child: Container(
          height: height,
          color: Colors.white,
        ),
      ),
    );
  }
}
