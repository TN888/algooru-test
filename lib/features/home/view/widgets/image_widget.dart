import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'image_error_widget.dart';
import 'image_loading_widget.dart';

class ImageWidget extends StatelessWidget {
  const ImageWidget(
    this.url, {
    Key? key,
    this.height,
  }) : super(key: key);

  final String url;
  final double? height;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(16),
        ),
      ),
      child: ClipRRect(
        borderRadius: const BorderRadius.all(
          Radius.circular(16),
        ),
        child: CachedNetworkImage(
          imageUrl: url,
          fit: BoxFit.cover,
          placeholder: (context, url) => ImageLoadingWidget(height: height),
          errorWidget: (context, url, error) => const ImageErrorWidget(),
        ),
      ),
    );
  }
}
