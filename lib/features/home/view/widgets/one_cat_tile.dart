import 'package:algooru_test/features/home/cubit/home_cubit.dart';
import 'package:algooru_test/features/home/view/widgets/image_error_widget.dart';
import 'package:algooru_test/features/home/view/widgets/image_loading_widget.dart';
import 'package:algooru_test/features/home/view/widgets/image_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class OneCatTile extends StatefulWidget {
  const OneCatTile({
    Key? key,
    required this.index,
  }) : super(key: key);

  final int index;

  @override
  State<OneCatTile> createState() => _OneCatTileState();
}

class _OneCatTileState extends State<OneCatTile> {
  @override
  void initState() {
    super.initState();

    context.read<HomeCubit>().getOneCat(widget.index);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeCubit, GeneralHomeState>(
      buildWhen: (previous, current) =>
          current is CatsState && current.index == widget.index,
      builder: (context, state) {
        if (state is CatsSuccess) {
          return ImageWidget(state.oneCat);
        } else if (state is CatsLoading) {
          return const ImageLoadingWidget();
        } else if (state is CatsFailure) {
          return const ImageErrorWidget();
        }
        return const SizedBox();
      },
    );
  }
}
