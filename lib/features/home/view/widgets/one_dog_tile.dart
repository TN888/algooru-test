import 'package:algooru_test/features/home/models/dogs_model.dart';
import 'package:algooru_test/features/home/view/widgets/image_widget.dart';
import 'package:flutter/material.dart';

class OneDogTile extends StatelessWidget {
  const OneDogTile(
    this.oneDogModel, {
    Key? key,
  }) : super(key: key);

  final OneDogModel oneDogModel;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: kElevationToShadow[2],
        borderRadius: BorderRadius.circular(16),
      ),
      child: Row(
        children: [
          Expanded(
            child: Container(
              margin: const EdgeInsetsDirectional.only(
                start: 4,
                top: 4,
                end: 16,
                bottom: 4,
              ),
              child: ImageWidget(
                oneDogModel.photo,
                height: 140,
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsetsDirectional.only(
                top: 16.0,
                end: 16,
                bottom: 16,
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  RichText(
                    maxLines: 2,
                    text: TextSpan(
                      text: 'Breed: ',
                      style: Theme.of(context).textTheme.bodyText2!.copyWith(
                            fontWeight: FontWeight.bold,
                          ),
                      children: [
                        TextSpan(
                          text: oneDogModel.breed,
                          style: Theme.of(context).textTheme.bodyText2,
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 10),
                  RichText(
                    maxLines: 2,
                    text: TextSpan(
                      text: 'Origin: ',
                      style: Theme.of(context).textTheme.bodyText2!.copyWith(
                            fontWeight: FontWeight.bold,
                          ),
                      children: [
                        TextSpan(
                          text: oneDogModel.origin,
                          style: Theme.of(context).textTheme.bodyText2,
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 10),
                  RichText(
                    maxLines: 3,
                    text: TextSpan(
                      text: 'Temperament: ',
                      style: Theme.of(context).textTheme.bodyText2!.copyWith(
                            fontWeight: FontWeight.bold,
                          ),
                      children: [
                        TextSpan(
                          text: oneDogModel.temperament.join(', '),
                          style: Theme.of(context).textTheme.bodyText2,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
