import 'package:algooru_test/features/home/view/widgets/one_cat_tile.dart';
import 'package:flutter/material.dart';

class CatsTab extends StatelessWidget {
  const CatsTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<bool>(
      initialData: true,
      future: Future.delayed(
        const Duration(milliseconds: 400),
        () => false,
      ),
      builder: (context, snapshot) {
        if (snapshot.data!) {
          return const SizedBox();
        }
        return GridView.builder(
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            crossAxisSpacing: 12,
            mainAxisSpacing: 12,
          ),
          physics: const BouncingScrollPhysics(
            parent: AlwaysScrollableScrollPhysics(),
          ),
          padding: const EdgeInsets.symmetric(
            vertical: 25,
            horizontal: 16,
          ),
          itemCount: 10,
          itemBuilder: (context, index) {
            return OneCatTile(index: index);
          },
        );
      },
    );
  }
}
