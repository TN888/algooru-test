import 'package:algooru_test/features/home/cubit/home_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../widgets/one_dog_tile.dart';

class DogsTab extends StatelessWidget {
  const DogsTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
          physics: const BouncingScrollPhysics(
            parent: AlwaysScrollableScrollPhysics(),
          ),
          padding: const EdgeInsets.symmetric(vertical: 25, horizontal: 16),
          child: BlocBuilder<HomeCubit, GeneralHomeState>(
            buildWhen: (previous, current) => current is DogsState,
            builder: (context, state) {
              if (state is DogsSuccess) {
                return Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    ListView.separated(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: state.dogsModel.dogBreeds.length,
                      itemBuilder: (context, index) {
                        return OneDogTile(state.dogsModel.dogBreeds[index]);
                      },
                      separatorBuilder: (context, index) {
                        return const SizedBox(height: 10);
                      },
                    ),
                    buildButton(context),
                  ],
                );
              } else if (state is DogsLoading) {
                return const Center(child: CircularProgressIndicator());
              } else if (state is DogsFailure) {
                return Text(
                  state.message,
                  textAlign: TextAlign.center,
                );
              }
              return const SizedBox();
            },
          ),
        ),
        bottomNavigationBar: BlocBuilder<HomeCubit, GeneralHomeState>(
          buildWhen: (previous, current) => current is DogsState,
          builder: (context, state) {
            if (state is DogsSuccess || state is DogsLoading) {
              return const SizedBox();
            }
            return buildButton(context);
          },
        ));
  }

  Widget buildButton(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: ElevatedButton(
        onPressed: () {
          context.read<HomeCubit>().getDogs();
        },
        child: const Text('Get dogs'),
      ),
    );
  }
}
