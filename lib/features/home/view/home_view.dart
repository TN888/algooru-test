import 'package:algooru_test/app.dart';
import 'package:algooru_test/features/home/cubit/home_cubit.dart';
import 'package:algooru_test/features/home/repository/home_repository.dart';
import 'package:algooru_test/features/home/view/tabs/dogs_tab.dart';
import 'package:algooru_test/global/dio/dio_client.dart';
import 'package:algooru_test/global/localization/localization_repository.dart';
import 'package:algooru_test/global/localization/translations.i18n.dart';
import 'package:algooru_test/global/widgets/keep_alive_widget.dart';
import 'package:algooru_test/global/widgets/my_dropdown_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:i18n_extension/i18n_widget.dart';

import 'tabs/cats_tab.dart';

class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => HomeCubit(
        HomeRepository(
          DioClient(),
        ),
      ),
      child: const HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Algooru Test'.i18n),
          actions: [
            Center(
              child: SizedBox(
                width: 80,
                child: MyDropdownField(
                  hint: LocalizationRepository().language,
                  items: languages.entries.map((e) => e.value.code).toList(),
                  onChanged: (value) async {
                    await LocalizationRepository().setLanguage(value!);
                    I18n.of(context).locale =
                        Locale(LocalizationRepository().language);
                    appStreamController.add(true);
                  },
                ),
              ),
            ),
            const SizedBox(width: 16),
          ],
          bottom: const TabBar(
            tabs: [
              Tab(text: 'Dogs'),
              Tab(text: 'Cats'),
            ],
          ),
        ),
        body: const TabBarView(
          children: [
            KeepAliveWidget(
              child: DogsTab(),
            ),
            KeepAliveWidget(
              child: CatsTab(),
            ),
          ],
        ),
      ),
    );
  }
}
