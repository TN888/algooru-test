// To parse this JSON data, do
//
//     final dogsModel = dogsModelFromMap(jsonString);

import 'dart:convert';

class DogsModel {
  DogsModel({
    this.dogBreeds = const [],
  });

  final List<OneDogModel> dogBreeds;

  factory DogsModel.fromJson(String str) => DogsModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory DogsModel.fromMap(Map<String, dynamic> json) => DogsModel(
        dogBreeds: List<OneDogModel>.from(
            json["dogBreeds"].map((x) => OneDogModel.fromMap(x))),
      );

  Map<String, dynamic> toMap() => {
        "dogBreeds": List<dynamic>.from(dogBreeds.map((x) => x.toMap())),
      };
}

class OneDogModel {
  OneDogModel({
    required this.breed,
    required this.breedType,
    required this.origin,
    required this.popularity,
    required this.temperament,
    required this.hypoallergenic,
    required this.intelligence,
    required this.photo,
  });

  final String breed;
  final String breedType;
  final String origin;
  final String popularity;
  final List<String> temperament;
  final String hypoallergenic;
  final int intelligence;
  final String photo;

  factory OneDogModel.fromJson(String str) =>
      OneDogModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory OneDogModel.fromMap(Map<String, dynamic> json) => OneDogModel(
        breed: json["breed"],
        breedType: json["breedType"],
        origin: json["origin"],
        popularity: json["popularity"],
        temperament: List<String>.from(json["temperament"].map((x) => x)),
        hypoallergenic: json["hypoallergenic"],
        intelligence: json["intelligence"],
        photo: json["photo"],
      );

  Map<String, dynamic> toMap() => {
        "breed": breed,
        "breedType": breedType,
        "origin": origin,
        "popularity": popularity,
        "temperament": List<dynamic>.from(temperament.map((x) => x)),
        "hypoallergenic": hypoallergenic,
        "intelligence": intelligence,
        "photo": photo,
      };
}
