import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'app.dart';
import 'global/localization/localization_repository.dart';
import 'global/repositories/user_repository.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  if (kReleaseMode) {
    debugPrint = (String? message, {int? wrapWidth}) {};
  }

  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  final userRepository = UserRepository();
  final localizationRepository = LocalizationRepository();

  final bool isFirstTime =
      await userRepository.getKey(firstTimeKey, defaultValue: true) as bool;
  if (isFirstTime) {
    await localizationRepository.setLanguage(languages['english']!.code);
    await userRepository.setKey(firstTimeKey, false);
  } else {
    await localizationRepository.getLanguage();
    debugPrint('Language ${LocalizationRepository().language}');
  }

  runApp(const AppPage());
}
