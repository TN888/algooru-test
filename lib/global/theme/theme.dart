import 'package:flutter/material.dart';

final lightTheme = ThemeData(
  primarySwatch: Colors.green,
  elevatedButtonTheme: _elevatedButtonThemeData,
);

final _elevatedButtonThemeData = ElevatedButtonThemeData(
  style: ButtonStyle(
    minimumSize: MaterialStateProperty.all(const Size(double.infinity, 40.0)),
    shape: MaterialStateProperty.all(
      const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(16.0),
        ),
      ),
    ),
  ),
);
