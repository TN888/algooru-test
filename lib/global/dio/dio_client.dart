import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

import 'app_interceptor.dart';

const String baseUrl = 'https://cataas.com';

class DioClient {
  DioClient._() {
    final baseOptions = BaseOptions(
      baseUrl: baseUrl,
      responseType: ResponseType.json,
    );

    _dio = Dio(baseOptions);
    _dio.interceptors.add(AppInterceptor());
    _dio.interceptors.add(
      PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        compact: false,
        maxWidth: 98,
        logPrint: myDioPrint,
      ),
    );
  }

  factory DioClient() {
    return _instance;
  }

  static final DioClient _instance = DioClient._();

  late final Dio _dio;

  Future<Response> getAll(
    String endpoint, {
    Map<String, dynamic>? queries,
    Map<String, dynamic>? headers,
  }) async {
    return _dio.get(endpoint,
        queryParameters: queries, options: Options(headers: headers));
  }

  Future<Response> getOne(
    String endpoint,
    String id, {
    Map<String, dynamic>? headers,
  }) async {
    return _dio.get('$endpoint/$id', options: Options(headers: headers));
  }

  Future<Response> post(
    String endpoint, {
    dynamic data,
    Map<String, dynamic>? headers,
  }) async {
    return _dio.post(endpoint, data: data, options: Options(headers: headers));
  }
}

void myDioPrint(Object object) {
  debugPrint('');
}
