import 'dart:io';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'language_model.dart';

const String languageKey = 'language key';

const Map<String, LanguageModel> languages = {
  'arabic': LanguageModel('العربية', 'ar'),
  'english': LanguageModel('English', 'en'),
};

bool isLanguage(BuildContext context, String language) {
  return LocalizationRepository().language == languages[language]!.code;
}

Future<String> getDeviceLanguage() async {
  String languageVar;
  if (Platform.localeName.substring(0, 2) == languages['arabic']!.code) {
    languageVar = languages['arabic']!.code;
  } else {
    languageVar = languages['english']!.code;
  }
  return languageVar;
}

class LocalizationRepository {
  factory LocalizationRepository() {
    return _instance;
  }
  LocalizationRepository._();
  static final _instance = LocalizationRepository._();

  String? _language;
  String fallbackLanguage = languages['english']!.code;

  Future<bool> setLanguage(String language) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final result = await prefs.setString(languageKey, language);

    if (result) _language = language;
    return result;
  }

  Future<String> getLanguage() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return _language = prefs.getString(languageKey) ?? fallbackLanguage;
  }

  String get language => _language ?? fallbackLanguage;

  String convertLanguageCodeToName(String language) {
    for (var i = 0; i < languages.values.length; i++) {
      if (languages.values.toList()[i].code == language) {
        return languages.values.toList()[i].name;
      }
    }
    throw Exception("Language name isn't found");
  }
}
