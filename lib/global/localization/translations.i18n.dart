import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations('en') +
      {
        'en': 'Algooru Test',
        'ar': 'الغورو تيست',
      };

  String get i18n => localize(this, _t);
}
