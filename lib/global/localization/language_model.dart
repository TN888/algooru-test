class LanguageModel {
  const LanguageModel(this.name, this.code);

  final String name;
  final String code;
}