import 'package:flutter/material.dart';

class MyDropdownField extends StatelessWidget {
  const MyDropdownField({
    Key? key,
    required this.hint,
    required this.items,
    required this.onChanged,
    this.value,
  }) : super(key: key);

  final String hint;
  final String? value;
  final List<String> items;
  final ValueChanged<String?>? onChanged;

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField(
      items: items
          .map(
            (e) => DropdownMenuItem<String>(value: e, child: Text(e)),
          )
          .toList(),
      onChanged: onChanged,
      hint: Text(hint),
      icon: const Icon(
        Icons.keyboard_arrow_down_rounded,
        color: Colors.white,
      ),
      iconSize: 30,
      value: value,
      decoration: const InputDecoration(
        isDense: true,
        contentPadding: EdgeInsets.symmetric(
          vertical: 3,
          horizontal: 10,
        ),
      ),
      selectedItemBuilder: (context) {
        return items
            .map(
              (e) => DropdownMenuItem<String>(value: e, child: Text(e)),
            )
            .toList();
      },
    );
  }
}
