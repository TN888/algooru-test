import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:i18n_extension/i18n_widget.dart';

import 'global/localization/localization_repository.dart';
import 'global/router/router.gr.dart';
import 'global/theme/theme.dart';

final appStreamController = StreamController<bool>();

class AppPage extends StatefulWidget {
  const AppPage({Key? key}) : super(key: key);

  @override
  State<AppPage> createState() => _AppPageState();
}

class _AppPageState extends State<AppPage> {
  final appRouter = AppRouter();

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<bool>(
      stream: appStreamController.stream,
      builder: (context, snapshot) {
        return Builder(
          builder: (context) {
            return I18n(
              initialLocale: Locale(LocalizationRepository().language),
              child: MaterialApp.router(
                title: 'Algooru Test',
                localizationsDelegates: const [
                  GlobalMaterialLocalizations.delegate,
                  GlobalWidgetsLocalizations.delegate,
                  GlobalCupertinoLocalizations.delegate,
                ],
                locale: Locale(LocalizationRepository().language),
                supportedLocales: languages.entries
                    .map<Locale>(
                      (mapEntry) => Locale(mapEntry.value.code),
                    )
                    .toList(),
                theme: lightTheme,
                debugShowCheckedModeBanner: false,
                routerDelegate: appRouter.delegate(),
                routeInformationParser: appRouter.defaultRouteParser(),
              ),
            );
          },
        );
      },
    );
  }
}
